var screen_height = $(window).height();
var viewMoreBtns = {
	'residential': true,
	'commercial': true,
	'debt': true,
}

var model_slider_data = null;



$(document).ready(function(){
	$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if(results) {
		localStorage.setItem('video', results[1]);

		return results[1] || 0;

	}
	return 0;
}

$(function(){

	if(localStorage.getItem('video') == 'ganesh' || localStorage.getItem('video') == 'krishnan' || localStorage.getItem('video') == 'sujan') {

		$('#'+localStorage.getItem('video')+'_testimonial').trigger('click');
		localStorage.removeItem('video');

	}
        
        // webinar registration code start
        var userLogin = typeof currentUser !== 'undefined' && currentUser ? true : false;
        var webinarreg = localStorage.getItem('webinarreg');
        if(typeof webinarreg != 'undefined'){
            if((webinarreg === 'true') && (userLogin === true)){
                localStorage.setItem('webinarreg', false);
                ga('send', 'event', 'WEBINARREGISTRATION', 'Submit');
                $.ajax({
                            url: 'webinar/register',
                            method: "POST",
                            data:{source:source},
                            success: function(response){
                            },
                            error: function(err){
                            }
                    });
                window.location.href = 'https://attendee.gotowebinar.com/register/1813872415207273473' + '?source=' + source;
            }
        }
        // webinar registration code end

});

// example.com?param1=name&param2=&id=6
	if($.urlParam('video')) {
		if($.urlParam('video') == 'brigade-tech-park') {
			$('.youtubeLinkModalLabel').html('INVESTMENT CASE VIDEO');
			$('#home-watch-video').modal('show');		
			$('#how_it_works_video').attr('src', 'https://www.youtube.com/embed/JH2selkn_JA');
			$('#how_it_works_video').attr('data-src', 'https://www.youtube.com/embed/JH2selkn_JA');
		}	

	} // name

		//Google Analytics Code Starts here//
	$('.select').select2({
		minimumResultsForSearch: Infinity
	});

	$('#trigger_activation_email_login').click(function() {
		var url = $('form[name="resend-activation-email-form-login"]').attr('action');
		var id =  $('#activation_email_link_login').val();
		var errLabel = $('form[name="signin"]').closest('.login-credentials').find('.errMsg').get(0);
		$('.login-credentials form .form-group').removeClass('inValid');
		$(errLabel).removeClass('inValid');
		$(errLabel).addClass('success');
		$(errLabel).text('Sending activation email..');
		$('#activation_email_button_login').css('display', 'none');
		var request = $.ajax({
        	url: url,
        	type: "POST",
        	dataType: "JSON",
        	data: {'id' : id }
      	});

      	request.done(function(msg) {
      		displaySuccessMsg(errLabel, msg['message']);
      	});

	});


	$('#appointlet').click(function() {
		$('#appointlet').attr('target', '_blank');
		$('#appointlet').attr('href', 'https://propertyshare.appointlet.com/s/investor-call/investor-relations-executive?email='+$('#investment-confirmation-email').val()+'&field__name='+$('#currentuser_name').val());
	});


	$('#investment-follow-up-details').click(function() {
		var btn = $(this);
		loading(btn);
		var data = {};
		data['type'] = $('#investment-type').val();	
		data['id'] = $('#invest-id').val();
		data['number'] = $('#investment-confirmation-number').val();
		data['email'] =  $('#investment-confirmation-email').val();
		data['country_code'] = $('#confirm-details-container').find('.selected-dial-code').text();
		if(data['country_code'].charAt(0) == '+'){
			data['country_code'] = data['country_code'].substr(1);
		}
		$('#save-details-error').css('display', 'none');
		$('.cnfm-content').css('display', 'block');
		var url = $('form[name="confirm-investment-form"]').attr('action');
			$.ajax({
				url: url,
				method: 'POST',
				dataType: "JSON",
				contentType: "application/json",
				data: JSON.stringify(data),
				success: function(response){
					$('.investment-success-message').css('display', 'none');
					stopLoading(btn);				
  					$('#confirm-details-container').css('display', 'none');
					$('#confirm-appointment-container').css('display', 'block');

				},
				error: function(err){
					$('#save-details-error').css('display', 'block');
					$('.cnfm-content').css('display', 'none');
					stopLoading(btn);
				}
			});

		console.log(data);
	});

	$('#trigger_activation_email_signup').click(function() {
		var url = $('form[name="resend-activation-email-form-signup"]').attr('action');
		var id =  $('#activation_email_link_signup').val();
		var errLabel = $('form[name="signup"]').closest('.login-credentials').find('.errMsg').get(0);
		$('.login-credentials form .form-group').removeClass('inValid');
		$(errLabel).removeClass('inValid');
		$(errLabel).addClass('success');
		$(errLabel).text('Sending activation email..');
		$('#activation_email_button_signup').css('display', 'none');
		var request = $.ajax({
        	url: url,
        	type: "POST",
        	dataType: "JSON",
        	data: {'id' : id }
      	});

      	request.done(function(msg) {
      		displaySuccessMsg(errLabel, msg['message']);
      });

	});

	(function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-64409350-1', 'auto');	
	ga('send', 'pageview');


	(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
		h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
		(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
		})(window,document.documentElement,'async-hide','dataLayer',4000,
		{'GTM-KW24LLP':true
	});

	if(typeof isLoggedIn !== 'undefined') {
		if(isLoggedIn == '') {
			$('#signin').modal('show');
			$('#signin').on('hidden.bs.modal', function () {
				if(! $('#signup').hasClass('in')) {
					window.location.href = redirectUrl;
				}
			});
			$('#signup').on('hidden.bs.modal', function () {
				if(! $('#signin').hasClass('in')) {
					window.location.href = redirectUrl;
				}			
			});
		}
	} 


	if(typeof login !== 'undefined') {
		if(login == 'Activated') {
        ga('send', 'event', 'ACTIVATE', 'Submit');
			$('#signin').modal('show');
		}
	}

	if($('.bars-section .graph ul li').length > 0){
		graphBars($('.bars-section .graph ul'));
	}


	$('#form-landing-page').submit(function( event ) {
        ga('send', 'event', 'LANDINGCLICK', 'Submit');
	});

	$('.prevent-report').click(function() {
		$('#signupModalLabel').html('Register to view the Reports!');
	});

	$('.prevent-models').click(function() {
		$('#signupModalLabel').html('Register to view the Models!');
	});

	$('.signup-modal-trigger').click(function() {
		$('#signupModalLabel').html('Register Now!');
	});

	$('.invest-button').click(function() {
		$('#signupModalLabel').html('Register to Invest!');
	});

	$('#watch_video_home').click(function() {
		$('#how_it_works_video').attr('src', 'https://www.youtube.com/embed/oH5LJ6w_8ic');
		$('#how_it_works_video').attr('data-src', 'https://www.youtube.com/embed/oH5LJ6w_8ic');
	});

	$('#agnel_testimonial').click(function() {
		$('#iframe_agnel').attr('src', 'https://www.youtube.com/embed/-Yf5LXarf_o');
		$('#iframe_agnel').attr('data-src', 'https://www.youtube.com/embed/-Yf5LXarf_o');		
	});

	$('#alden_testimonial').click(function() {
		$('#iframe_alden').attr('src', 'https://www.youtube.com/embed/6umsIIoxMNc');
		$('#iframe_alden').attr('data-src', 'https://www.youtube.com/embed/6umsIIoxMNc');		
	});

	$('#ganesh_testimonial').click(function() {
		$('#iframe_ganesh').attr('src', 'https://www.youtube.com/embed/XkMmal4Vw78');
		$('#iframe_ganesh').attr('data-src', 'https://www.youtube.com/embed/XkMmal4Vw78');		
	});

	$('#krishnan_testimonial').click(function() {
		$('#iframe_krishnan').attr('src', 'https://www.youtube.com/embed/5jK51_g3W2M');
		$('#iframe_krishnan').attr('data-src', 'https://www.youtube.com/embed/5jK51_g3W2M');		
	});
	$('#sujan_testimonial').click(function() {
		$('#iframe_sujan_murahari').attr('src', 'https://www.youtube.com/embed/Z0vGR8zuNqg');
		$('#iframe_sujan_murahari').attr('data-src', 'https://www.youtube.com/embed/Z0vGR8zuNqg');		
	});

	$('#watch_video_how_it_works').click(function() {
		$('#iframe_alden').attr('src', 'https://www.youtube.com/embed/oH5LJ6w_8ic');
		$('#iframe_alden').attr('data-src', 'https://www.youtube.com/embed/oH5LJ6w_8ic');
	});

	$('input.onlyNumber').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	
	if(screen.width > 768 && $('.screen-cover-container').length > 0){
		$('.landing').removeClass('visibility-hidden');
		$('.screen-cover-container').css({
			'height': screen_height + 'px'
		});
	}else{
		$('nav#menu').removeClass('visibility-hidden');
		$('nav#menu').mmenu({
			"extensions": [
              "effect-menu-zoom",
              "effect-panels-zoom",
              "pagedim-black"
           	],
	         "offCanvas": {
	            "position": "right"
	         },
	         
      	});
      	
      	$("li.cross span").click(function() {
	        MmenuClose();
	    });
	}

	if(screen.width > 768){
		sameHeight();
	}

	if($('.card-slider').length > 0){
		applyCardSlider();
	}
	if($('.model-slider').length >0){
		var model_slider = $('.model-slider').slider({
			formatter: function(value) {
				return value + '%';
			}
		});
		$(model_slider).on('change', function(e){
			var values = e.value;
			var newValue = values.newValue;
			
			if(model_slider_data[newValue]){
				var obj = model_slider_data[newValue];
				for(var key in obj){
					if(key != 'sales-number' && key != 'sales-value'){
						$('.'+key).find('.value').html(obj[key]);
					}else if(key == 'sales-number'){
						$('.sales-value').find('.value').html(convertToIndianSystem(obj[key]));
					}
				}
				var sales_number = obj['sales-number'];
				var rit = $('.sales-value-total').data('rit');
				var sales_value_total = convertToIndianSystem(Number(sales_number) + Number(rit));
				$('.sales-value-total').find('.value').html('&#8377; '+ sales_value_total);
				$('.query.sale').attr('data-content', 'Assuming '+newValue+'% annual appreciation');
			}
		});
	}
	if($('.tenancy-slider').length > 0){
		var model_slider = $('.tenancy-slider').slider({
			// formatter: function(value) {
			// 	return value + '';
			// }
		});
	}
	if($('#investNow-modal').length > 0){
		var detail_slider, activeSlider, data, getValue;
		$('#investNow-modal .checkbox').off('click');
		$('#investNow-modal .checkbox').click(function(){
			$('label.validation').removeClass('inValid');
			$('.cnfm-invest').removeClass('inValid');
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$(activeSlider).slider('setValue', getValue);
				calOwnerShip(getValue, data);
			}else{
				$(this).addClass('active');
				getValue = $(activeSlider).slider('getValue');
				$(activeSlider).slider('setValue', data.active);
				calOwnerShip(data.active, data);
			}
		});
		$('#investNow-modal .number-box').keyup(function(e){
			data = $('#slider-data-content').data();
			var value = $(this).find('input').val();
			if(value){
				value = JSON.parse($('#investNow-modal .number-box input').val().replace(/,/g, ''));
				$('#investNow-modal .number-box input').val(convertToIndianSystem(value));
				var inValid = false;
				var text = '';
				$('#investNow-modal .number-box input').attr('data-value', value);
				var sliderInputData = $('#bid_now_input_box').data();
				var step = Number(sliderInputData.sliderstep);
				var text = $('#bid_now_input_box').attr('data-errtext');
				if(value % step != 0){
					inValid = true;
					// text = 'Amount should be in multiple of lakhs';
				}
				if(value > data.active){
					inValid = true;
					text = sliderInputData.errmax;
				}
				if(value < data.minimum){
					inValid = true;
					text = sliderInputData.errmin;
				}
				if(isNaN(value) || inValid){
					$('label.validation').html(text);
					$('label.validation').addClass('inValid');
					$('.cnfm-invest').addClass('inValid');
				}else{
					$('label.validation').removeClass('inValid');
					$('.cnfm-invest').removeClass('inValid');
					$(activeSlider).slider('setValue', value);
					calOwnerShip(value, data);
				}
			}
		});
		var sliderApplied = true;
		
		$('#investNow-modal').on('shown.bs.modal', function() {
			if(sliderApplied){
				data = $('#slider-data-content').data();
				var totalWidth = $('.invest-slider-wrapper').width();
				var sliderTotal = data.total - data.minimum;;
				if(data.type == 'debt'){
					sliderTotal = data.loanamount - data.minimum;
				}
				detail_slider = $('.detail-slider').slider({
					formatter: function(value) {
						if(value === data.active){
							$('#investNow-modal .checkbox').addClass('active');
						}else{
							$('#investNow-modal .checkbox').removeClass('active');
						}
						return value;
					}
				});
				activeSlider = $(detail_slider).get(1);
				$(activeSlider).on('change', function(e){
					$('label.validation').removeClass('inValid');
					$('.cnfm-invest').removeClass('inValid');
					var value = e.value;
					calOwnerShip(value.newValue, data);
				});
				$('#choose-yield').on('change', function(){
					var selected_bid = $(this).val();
					var yieldObj = _.find(data['bid'], function(item) {
    					return item.id == selected_bid;
					});	
					console.log(yieldObj);				
					$('.slider-detail.bid_now .detail-slider').attr('data-slider-max', yieldObj['offer_price']);
					$('.slider-detail.bid_now .detail-slider').attr('data-slider-min', yieldObj['min_investment']);
					$('.slider-detail.bid_now .detail-slider').attr('data-slider-step', yieldObj['increments']);
					$(activeSlider).slider('setAttribute', 'max', Number(yieldObj['offer_price']));
					$(activeSlider).slider('setAttribute', 'min', Number(yieldObj['min_investment']));
					$(activeSlider).slider('setAttribute', 'step', Number(yieldObj['increments']));
					$(activeSlider).slider('setAttribute', 'value', Number(yieldObj['min_investment']));
					$(activeSlider).slider('refresh');
					var prevWidth = $('.slider-detail.active .slider-horizontal').width();
					$('.slider-detail.active .slider-horizontal').width(prevWidth+'px');
					$('#investNow-modal .number-box input').attr('data-value', yieldObj['min_investment']);
					$('#investNow-modal .number-box input').val(convertToIndianSystem(yieldObj['min_investment']));
					$('#slider-data-content').data('active', yieldObj['offer_price']);
					$('.slider-detail.bid_now .text-label.top-left').text(numberText(yieldObj['min_investment']));
					$('.slider-detail.bid_now .text-label.top-right').text(numberText(yieldObj['offer_price']));
					$('#selected_yield').val(yieldObj['id']);
					$('#rental_yield_percentage').val(yieldObj['yield']);
					$('#bid_now_input_box').attr('data-errtext', 'Amount should be in multiple of '+ convertToIndianSystem(yieldObj['increments']));
					calOwnerShip(yieldObj['min_investment'], data);
					$('label.validation').html('');
				});
				$(activeSlider).slider('refresh');
				var initWidthPercent = 0;
				var initWidth = (initWidthPercent * totalWidth) / 100;
				var activeWidthPercent = ((data.active - data.minimum) / sliderTotal) * 100;
				var activeWidth = (activeWidthPercent * totalWidth) / 100;
				var disabledWidthPercent = (data.disabled / sliderTotal) * 100;
				var disabledWidth = (disabledWidthPercent * totalWidth) / 100;
				$('.slider-detail.init .slider-horizontal').width(initWidth+'px');
				$('.slider-detail.init img.dot').css({
					left: parseInt(initWidth - 9) + 'px'
				});
				$('.slider-detail .top-left').css({
					left: parseInt(initWidth - 30) + 'px'
				});
				$('.slider-detail .bottom-left').css({
					left: parseInt(initWidth - 46) + 'px'
				});
				$('.slider-detail.active .slider-horizontal').width(activeWidth+'px');
				$('.slider-detail.disabled .slider-horizontal').width(disabledWidth+'px');
				$('.slider-detail.active img.right').css({
					'right': (disabledWidth) + 'px'
				});
				sliderApplied = false;
			}
		});
		
		$('#investNow-modal .cnfm-invest button').off('click');
		$('#investNow-modal .cnfm-invest button').click(function(){
			var ele = $(this);
			$(ele).addClass('inValid');
			var data = $(ele).data();
			var url = data.url;
			var names = data.names;
			var body = {};
			body[names.amountname] =  Number($('#investNow-modal .number-box input').attr('data-value'));
			if(names.amountname == 'BidAmount') {
				body['bid_id'] = $('#selected_yield').val();
				ga('send', 'event', 'BIDNOW', 'Submit');
			}
			if(names.amountname == 'investmentAmount') {
				ga('send', 'event', 'INVESTNOW', 'Submit');
			}
			body['investOption']	= names.investOption;
			$('#investNow-modal').modal('toggle');
			$('#investNow-modal-cnfm').modal('toggle');
			$('.investment-success-message').css('display', 'block');
			$('#confirm-appointment-container').css('display', 'none');
			$(ele).removeClass('inValid');
			$.ajax({
				url: url,
				method: 'POST',
				dataType: "JSON",
				contentType: "application/json",
				data: JSON.stringify(body),
				success: function(response){
					$('#loading_gif').css('display', 'none');
					$('#confirm-details-container').css('display', 'block');
					$('#invest-id').val(response['id']);
					$('#investment-type').val(response['type']);
					console.log('Request Successful')
				},
				error: function(err){
					console.log(err)
				}
			});
		});
	}
	if($('.bxslider').length > 0){
		var opts = {};
		if(screen.width <= 768){
			var slideWidth = screen.width - 20;
			if(screen.width == 768){
				opts.slideMargin = 20;
				opts.minSlides = '2';
				opts.maxSlides = '3';
			}
			opts.slideWidth = slideWidth;
		}
		$('.bxslider').bxSlider(opts);
	}
	if($('.overview-location .map').length >0){
		var data = $('.overview-location .map').data();
		initializeMap(data.latitude, data.longitude, $('.overview-location .map').get(0), data.icon);
	}
	if($('.contact-container .map').length >0){
		var data = $('.contact-container .map').data();
		initializeMap(data.latitude, data.longitude, $('.contact-container .map').get(0), data.icon);
 	}
	if($('.overview-financial .bar-graph').length > 0){
		barGraph($('.overview-financial .bar-graph'), true)
	}
	if($('.overview-price-box .pie-chart').length > 0){
		overviewPie();
	}

	$('.panel-collapse').on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    })

    $('.listing-title-list li').click(function(){
    	$(this).addClass('active').siblings().removeClass('active');
    });

    $('.listing-sortby ul li').click(function(){
    	var index = $(this).index();
    	var left = (index * 50) + '%';
    	$(this).closest('.listing-sortby').find('.shadow').css({
    		'left': left
    	});
		$(this).addClass('active').siblings().removeClass('active');
    });

    if($('.faqs-list ul li').length > 0){
    	$('.faqs-list ul li').click(function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		var data = $(this).data();
    		var tab = data.tab;
    		$('.faqs-list-content[data-tab="'+tab+'"]').addClass('active').siblings().removeClass('active');
    	});
    }
    if($('.modal').length > 0){
		$('.modal').on('shown.bs.modal', function() {
		    var initModalHeight = $(this).find('.modal-dialog').outerHeight(); //give an id to .mobile-dialog
		    var userScreenHeight = screen_height;
		    if (initModalHeight > userScreenHeight) {
		        $(this).find('.modal-dialog').css('overflow', 'auto'); //set to overflow if no fit
		    } else {
		        $(this).find('.modal-dialog').css('margin-top', 
		        (userScreenHeight / 2) - (initModalHeight/2)); //center it if it does fit
		    }
		    $('body').addClass('modal-open');
		});
		$('.modal').on('hidden.bs.modal', function() {
			$('body').removeClass('modal-open');
		});
	}
	if($('.user-phone').length > 0){
		var countryData = $.fn.intlTelInput.getCountryData();
		var dialCode = JSON.stringify($('.user-phone').data('dialcode'));
		var initialCountryCode = '';
		if(!dialCode || dialCode == '""'){
			initialCountryCode = 'in';
			$('.user-phone').intlTelInput({
			'initialCountry': "auto",
			geoIpLookup: function(callback) {
    			$.ajax({
					method: 'GET',
					url: 'https://freegeoip.net/json/?callback=',
				}).done(function(result) {
					callback(result.country_code);
				});
  			}
		});
		}else{
			var obj = _.find(countryData, { "dialCode": dialCode });
			var initialCountryCode = obj.iso2;
			$('.user-phone').intlTelInput({
				'initialCountry': initialCountryCode,
			});
		}
		$('.user-phone').intlTelInput({
			'initialCountry': "auto",
			geoIpLookup: function(callback) {
    			$.ajax({
					method: 'GET',
					url: 'https://freegeoip.net/json/?callback=',
				}).done(function(result) {
					callback(result.country_code);
				});
  			}
		});
	}

	$('.openLogin').click(function(){
		$('#signup').modal('toggle');
	});

	$('.openSigin').click(function(){
		$('#signin').modal('toggle');
	});

	$('[data-toggle="popover"]').popover({
		container: 'body'
	});
	$('.pricing-toggle-img').click(function(){
		$('.toggle-price').toggleClass('hide');
	});
	loginInputAnimate();

	if ($('.model-list-ul').length > 0) {
		var modelData = $('.model-list-ul').data();
		model_slider_data = modelData.slidevalues;
	}
	if($('.play-btn').length > 0){
		$('.play-btn').click(function(){
			var index = $(this).closest('.investor-testimonials-li').data('index');
			var modal = $('.modal.'+index).get(0);
			$(modal).modal('toggle');
			var youtubeLink = $(modal).find('iframe').data('src');
			var youtubeLinkPlay = youtubeLink + '?autoplay=1';
			$(modal).find('iframe').attr('src', youtubeLinkPlay);
		});
		$('.investor-testimonials-modal').on('hidden.bs.modal', function() {
			var modal = $(this).get(0);
			var youtubeLink = $(modal).find('iframe').data('src');
			$(modal).find('iframe').attr('src', '');
			$(modal).find('iframe').attr('src', youtubeLink);
		});
	}

	if($('#home-watch-video').length > 0){
		$('#home-watch-video').on('shown.bs.modal', function() {
			var youtubeLink = $(this).find('.video').data('src');
			var youtubeLinkPlay = youtubeLink + '?autoplay=1';
			$(this).find('.video').attr('src', youtubeLinkPlay);
		});
		$('#home-watch-video').on('hidden.bs.modal', function() {
			var youtubeLink = $(this).find('.video').data('src');
			$(this).find('.video').attr('src', youtubeLink);
		});
	}
	
	$('form[name="signin"]').submit(function(e){
		e.preventDefault();
		signin();
	});
	$('form[name="signup"]').submit(function(e){
		e.preventDefault();
		signup();
	});
	$('form[name="forgotPassword"]').submit(function(e){
		e.preventDefault();
		forgotPassword();
	});
	$('form[name="updateProfile"]').submit(function(e){
		e.preventDefault();
		updateProfile();
	});
});

if(screen.width > 768 && $('.investor-testimonials-li').length > 3){
	$('.investor-testimonials-li').closest('ul').addClass('bxslider').removeClass('no-bxslider');
	var testimonialsOptions = {
		'minSlides': 3,
		'maxSlides': 3,
		'slideWidth': 340,
		'infiniteLoop': false,
		'hideControlOnEnd': true,
		'slideMargin': 40,
		'pager': false
	}
	$('.investor-testimonials-ul.bxslider').bxSlider(testimonialsOptions);
}else if(screen.width <= 768){
	$('.investor-testimonials-ul').removeClass('col-lg-9 col-md-10');
	var testimonialsOptions = {};
	var pager = true;
	if(screen.width == 768){
		var slideWidth = screen.width - 20;
		testimonialsOptions.slideMargin = 20;
		testimonialsOptions.slideWidth = slideWidth;
		testimonialsOptions.minSlides = '2';
		testimonialsOptions.maxSlides = '3';
		if($('.investor-testimonials-li').length == 2){
			pager = false;
			testimonialsOptions.pager = pager;
		}
	}
	$('.investor-testimonials-ul').bxSlider(testimonialsOptions);
}


$('.login-credentials').on('shown.bs.modal', function() {
	loginInputAnimate();
});

$(document).on('show.bs.modal','#signup', function () {
	fetchAndUpdateUserLocation();
    $('#leadsquared_cookie_signup').val(checkCookie());
});

$(document).on('show.bs.modal','#signin', function () {
	fetchAndUpdateUserLocation();
    $('#leadsquared_cookie_login').val(checkCookie());
});

/* Changes done by Anurag Gaur Start */

// To get query parameter from current url
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// checking for investers meet campaign request and showing thank you popup
$(function(){
    if(getParameterByName('place') && (getParameterByName('campaign') === 'investor_meet') && !getCookie(getParameterByName('place')+'-requested-for-investor-meet')){
        setCookie(getParameterByName('place')+'-requested-for-investor-meet',getParameterByName('place'),1);
        $('#request-confirmation-modal').modal({backdrop: 'static', keyboard: false});
        setTimeout(function(){
            window.location = '/';
        }, 5000);
    }

    if(getParameterByName('cId') === 'b42312' && getParameterByName('source') === 'email'){
        $('#request-confirmation-modal').modal({backdrop: 'static', keyboard: false});
        setTimeout(function(){
            window.location = window.location.href.split('?')[0] + '?option=invest';
        }, 5000);
    }
});

// setting the cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/* Changes done by Anurag Gaur End */

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var ProspectID = getCookie("ORG13881");
    return ProspectID;
}


function MmenuClose(){
	var Mmenu = $('nav#menu').data( "mmenu" );
	Mmenu.close();
}

function mobileLogin(){
	MmenuClose();
	$('#signin').modal('toggle');
}

function loginInputAnimate(){
	$(".mat-input").focus(function(){
	  $(this).parent().addClass("is-active is-completed");
	});

	$(".mat-input").focusout(function(){
	  if($(this).val() === "")
	    $(this).parent().removeClass("is-completed");
	  $(this).parent().removeClass("is-active");
	});
}

function sameHeight(){

	$('.eq-ht-ul').each(function(i, ul){
		var max = -1;
		$(ul).find('li.eq-ht-li').each(function(j, li){
			// console.log($(li))
			var h = $(li).height();
			if($(li).hasClass('pricing')){
				h = h + 40
			}
			max = h > max ? h : max;
		});
		
		if($(ul).closest('.tab-pane').hasClass('active')){
			$(ul).find('li.eq-ht-li').height(max + 'px');
		}
	});
}

function numberText(x){
	var result = '';
	if(x >= 10000000){
		result = Math.round(x / 10000000);
		if(result % 1 != 0){
			result = result.toFixed(2);
		}
		result = result + ' Cr';
	}else if(x >= 100000 && x < 10000000){
		result = Math.round(x / 100000);
		if(result % 1 != 0){
			result = result.toFixed(2);
		}
		result = result + ' L';
	}else if(x >= 1000 && x < 1000000){
		result = Math.round(x / 1000);
		if(result % 1 != 0){
			result = result.toFixed(2);
		}
		result = result + ' K';
	}
	return result;
};

function timelineUpdates(){
	var timelineBlocks = $('.cd-timeline-block'),
		offset = 0.8;

	//hide timeline blocks which are outside the viewport
	hideBlocks(timelineBlocks, offset);

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
		(!window.requestAnimationFrame) 
			? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
			: window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
	});

	function hideBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		});
	}

	function showBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
		});
	}
}
function graphBars(ele){
	var data = $(ele).find('.bid_bars').data('bid_bars');
	var maxNumberObj = _.max(data, function(obj){ return Number(obj.number); });
	var bigNumber = maxNumberObj.number;
	var maxHeight = 270;
	var newHeight = maxHeight;
	$('.bars-section .graph ul li').each(function(i, li){
		var number = $(li).find('.number').data('number');
		if(number != bigNumber){
			newHeight = ( number * maxHeight ) / bigNumber;
		}else{
			newHeight = maxHeight;
		}
		$(li).find('.bar-line').height(newHeight);
	});
}

function viewMoreProperties(eleBtn){
	var data = $('.cards.active').data();

	var ele = $('.cards.active .list-main').get(0);
	var listingCategory = data.listing;
	var host = location.host;
	var defaultUrl = 'http://'+host+'/main/public/api/properties/'+listingCategory+'properties/properties?page=2';
	var siteUrl = $(eleBtn).attr('data-url');
	$(eleBtn).removeAttr('data-url');
	if(!siteUrl){
		siteUrl = defaultUrl;
	}
	$.get(siteUrl, function(response){
		// console.log(response)
		var properties = response.data;
		createCardItem(properties, listingCategory, ele, host);
		if(response.next_page_url){
			$(eleBtn).attr('data-url', response.next_page_url);
		}else{
			delete viewMoreBtns[listingCategory];
			$(eleBtn).addClass('hide');
		}
	}, 'json')
}

function createCardItem(data, listingCategory, ele, host){
	for (var i = 0; i < data.length; i++) {
		(function(i, property){
			var TotalPrice = 0;
			var property = data[i];
			var days = null;
			var link = 'http://'+host+'/main/public/properties/'+listingCategory+'properties/'+property.id+'/view';
			if(property.property_details.expiry_date){
				var expiry_date = new Date(property.property_details.expiry_date);
				var today = new Date();
				days = Math.ceil((expiry_date - today)/1000/60/60/24);
				if(days < 0){
					days = null;
				}
			}
			for(var key in property.all_in_price) {
				if ( key !== 'area' && key !== 'per_square_feet' && property.all_in_price[key] != '0' ) {
					TotalPrice += Math.round(property.all_in_price[key]);
				}
			}
			var funded =  ((Math.round(property['funds']['funded']) * 100) / TotalPrice).toFixed(2);
			var image;
			var features = [];
			property.files.forEach(function(file){
				if(file.pivot.zone == 'property_list_thumbnail'){
					image = file;
				}else if(file.pivot.zone.indexOf('features') != -1){
					var obj = {};
					obj.link = file.path_string;
					var key = file.pivot.zone.split('_');
						key = key[key.length -1];
					if(property.features && property.features[key]){
						obj.text = property.features[key].title;
					}
					features.push(obj);
				}
			});
			var iconsData = getListingIconsNData(property);
			// console.log(iconsData)
			var dataTemplate = { 
				property: property, 
				TotalPrice: TotalPrice, 
				funded: funded, 
				image: image, 
				days: days,
				i: i,
				features: features,
				listingType: listingCategory,
				iconsData: iconsData,
				link: link 
			};
			$.get('themes/flatly/js/templates/listing.ejs?t='+ new Date().getTime(), function (template) {
				var func = ejs.compile(template);
				var html = func(dataTemplate);
				$(ele).append(html);
				applyCardSlider();
			});
		})(i, data[i])
	}
}

function getListingIconsNData(property){
	var arr = [];
	var expectedObj = '';
	if(property['slider_values'] && property['model_details']['slider']['expected']){
		expectedObj = property['slider_values'][property['model_details']['slider']['expected']];
	}
	if(property['type'] == "equity"){
		arr[0] = {};
		arr[0]['name'] = 'RENTAL YIELD';
		arr[0]['img_name'] = 'rental-yield-black';
		arr[0]['value'] = property['rental_yields']['yield'] + ' %';

		arr[1] = {};
		arr[1]['name'] = 'RETURN TARGET';
		arr[1]['img_name'] = 'return-target-black';
		arr[1]['value'] = expectedObj['irr'];

		arr[2] = {};
		arr[2]['name'] = 'MIN INVESTMENT';
		arr[2]['img_name'] = 'minimum-investment-black';
		arr[2]['value'] = '&#8377; ' + convertToIndianSystem(property['funds']['minimum_investment']);
	}else if(property['type'] == "debt"){
		arr[0] = {};
		arr[0]['name'] = 'INTEREST';
		arr[0]['img_name'] = 'interest-rate-black';
		arr[0]['value'] = property['debt']['interest_rate'] + ' %';

		arr[1] = {};
		arr[1]['name'] = 'TENURE';
		arr[1]['img_name'] = 'tenure-black';
		var yr = (property['debt']['tenture'] > 1) ? ' YEARS' : ' YEAR';
		arr[1]['value'] = property['debt']['tenture'] + yr;

		arr[2] = {};
		arr[2]['name'] = 'LTV';
		arr[2]['img_name'] = 'ltv-black';
		arr[2]['value'] = Math.round(( property['debt']['loan_amount'] / property['all_in_price']['purchase_price']) * 100, 2) + ' %';
	}
	return arr;
}

function applyCardSlider(){
	$('.card-slider').slider({
		formatter: function(value) {
			return value + '%';
		}
	});
}

function calOwnerShip(value, data){
	data['yield'] = Number($('#rental_yield_percentage').val());
	if(value && data){
		// console.log(value, data)
		if(data.status != 'bid_now'){
			var total = data.total;
			var monthlyRent = data.monthlyrent;
			if(data.type != 'debt'){
				var fractional_ownership_percent = ((value / total) * 100).toFixed(1);
                                var propertyAllInPrice = data.allinprice;
                                var propertyInitialInvestment = total;

                                // calculation of monthly rent is based on allinprice * ((investmentamount * rentalyield) / 1200) / initialinvestment
				var monthlyRentNew = convertToIndianSystem(Math.round( propertyAllInPrice * ((value * data.yield) / 1200) / propertyInitialInvestment));
			}
			else{
				total = data.loanamount;
				var debt_model = $('#debt_model').val();
				debt_model = JSON.parse(debt_model);
				var amortization = Number(debt_model[2]['amortization']);
				var interest = Number(debt_model[2]['interest']);
				var emi = (value/data.loanamount)*(amortization+interest)/12;
				var fractional_ownership_percent = ((value / data.loanamount) * 100).toFixed(1);
				var monthlyRentNew = convertToIndianSystem(Math.round( emi));
			}
			
			
			
		}else if(data.status == 'bid_now'){
			var bidArray = data['bid'];
			var current_monthly_rent = bidArray[0]['current_monthly_rent'];
			var current_estimated_property_value = bidArray[0]['current_estimated_property_value'];
			bidArray = _.map(bidArray, function(obj){ 
				obj['current_monthly_rent'] =  current_monthly_rent;
				obj['current_estimated_property_value'] =  current_estimated_property_value;
				return obj;
			});
			var selectedYield = Number($('#choose-yield').val());
			var bid_obj = _.find(bidArray, function(item) {
    			return item.id == selectedYield;
			});
	
			$('.slider-result-yield .value').text(bid_obj.yield+'%');
			var fractional_ownership_percent = ((bid_obj['customer_investment'] * value * 100) / (data.total_parent * bid_obj['offer_price'])).toFixed(1);
			var monthlyRentNew = Math.round(value * bid_obj['yield']/(100*12));
				monthlyRentNew = convertToIndianSystem(monthlyRentNew);
		}
		$('#investNow-modal .number-box input').attr('data-value', value);
		$('#investNow-modal .number-box input').val(convertToIndianSystem(value));
		$('.fractional_ownership_text').text(fractional_ownership_percent + '%');
			monthlyRentNew = '&#8377; ' + monthlyRentNew;
		$('.current_monthly_rent_text').html(monthlyRentNew);
	}
}

function pricingPie(ele){
	$(ele).removeAttr('onclick');
	var data = $(".pricing-content .pie-chart").data();
		data = data.piechart;
	var dataArray = pieChart(data);
	setTimeout(function(){
		$(".pricing-content .pie-chart").piechart(dataArray);
		barGraph($('.rental-container .bar-graph'), false);
		sameHeight(true);
	},400)
}

function overviewPie(){
	var data = $('.overview-price-box .pie-chart').data();
		data = data.piechart;
	// console.log(data)
	var dataArray = pieChart(data);
	$(".overview-price-box .pie-chart").piechart(dataArray);
}

function pieChart(data){
	var dataArray = new Array;
	dataArray.push(["Name","Number", "color"]);
	for(var key in data){
		var arr = new Array();
		arr.push(key);
		arr.push(parseInt(data[key].number));
		arr.push(data[key].color);
		dataArray.push(arr);
	}
	return dataArray;
}

function barGraph(elemain, reverse){
	var barBoxList;
	if(!reverse){
		barBoxList = $(elemain).find('ul li.nt-gross .bar-box');
	}else{
		barBoxList = $(elemain).find('ul li.nt-gross .bar-box').toArray().reverse()
	}
	var data = $(elemain).data();
		data = data.bargraph;
	var grossHeight = $(elemain).find('ul li.gross .bar-box').height();
	var grossLiHeight = $(elemain).find('ul li.gross').height();
	$(elemain).find('ul li').height(grossLiHeight);
	var grossAmount = JSON.parse((data.gross_rent) ? data.gross_rent.number : data.total.number);
	var marginTop = 0;
	$(barBoxList).each(function(i, ele){
		var key = $(ele).data('key');
		var keyNumber = parseInt(data[key].number);
		var numberPercent = (keyNumber / grossAmount) * 100;
		var numberHeight = (numberPercent * grossHeight) / 100;
		$(ele).height(numberHeight);
		$(ele).css({
			'margin-top': marginTop
		});
		$(ele).parent().find('.name').css({
			'bottom': 'calc(-60px + '+marginTop+'px)'
		});
		marginTop = marginTop + numberHeight;
	});
}


function listingAction(ele){
 	var data = $(ele).data();
	// $('.cards').removeClass(data.othergrid + '-grid');
	// $('.cards').addClass(data.grid + '-grid');
	$('.cards[data-listing="'+data.name+'"]').addClass('active').siblings().removeClass('active');
	
	if(viewMoreBtns[data.name]){
		$('.view-all button').removeClass('hide');
	}else{
		$('.view-all button').addClass('hide');
	}
}

function mapDetails (ele){
	var id = $(ele).attr('href');
	var data = $(ele).data();
	var eleMap = $(id).find(id+'-map').get(0);
	var key = $(id).attr('id');
	var Lat = data.latitude;
	var Long = data.longitude;
	var icon = data.icon;
	setTimeout(function(){
		initializeMap(Lat, Long, eleMap, icon);
		$(ele).removeAttr('onclick')
	},400);
}
function initializeMap(Lat, Long, eleMap, markerImg){
	var icon = {
		url: markerImg,
		scaledSize: new google.maps.Size(50, 65),
	}
	var myLatlng = new google.maps.LatLng(Lat, Long);
	var myOptions = {
		zoom: 13,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(eleMap, myOptions);
	var marker = new google.maps.Marker({
	    position: myLatlng,
	    icon: icon,
	});
	marker.setMap(map);
}
function convertToIndianSystem(x){
	if(x){
    		x=x.toString();
        var afterPoint = '';
        if(x.indexOf('.') > 0)
           afterPoint = x.substring(x.indexOf('.'),x.length);
        x = Math.floor(x);
        x=x.toString();
        var lastThree = x.substring(x.length-3);
        var otherNumbers = x.substring(0,x.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
        return res;

	}else
		return 0;
}



// login credentials

function signin(){
	var btn = $('form[name="signin"]').find('button[type="submit"]').get(0);
	loading(btn);
	var arr = $('form[name="signin"]').serializeArray();
	var errLabel = $('form[name="signin"]').closest('.login-credentials').find('.errMsg').get(0);
	$('.login-credentials form .form-group').removeClass('inValid');
	$(errLabel).removeClass('inValid');
	$(errLabel).removeClass('success');
	$(errLabel).text('');
	var data = {};
	for (var i = 0; i < arr.length; i++) {
		var obj = arr[i];
		data[obj.name] = obj.value;
	}
	var url = $('form[name="signin"]').attr('action');
	var action = 'LOGIN';
	callAjax(url, data, errLabel, btn, window.location.href, null, action);
}

function signup(){
	var btn = $('form[name="signup"]').find('button[type="submit"]').get(0);
	loading(btn);
	var arr = $('form[name="signup"]').serializeArray();
	var errLabel = $('form[name="signup"]').closest('.login-credentials').find('.errMsg').get(0);
	$('.login-credentials form .form-group').removeClass('inValid');
	$(errLabel).removeClass('inValid');
	$(errLabel).removeClass('success');
	$(errLabel).text('');
	var data = {};
	for (var i = 0; i < arr.length; i++) {
		var obj = arr[i];
		data[obj.name] = obj.value;
	}
	var number = $('form[name="signup"]').find('.selected-dial-code').text();
	if(number.charAt(0) == '+'){
		number = number.substr(1);
	}
	data['country_code'] = number;
	var url = $('form[name="signup"]').attr('action');
	//var successMsg = "A link has been sent to your registered email to active your account";
	var successMsg = 'Successfully signed up'
	var action = "SIGNUP";
	callAjax(url, data, errLabel, btn, null, successMsg, action);
}

function forgotPassword(){
	var btn = $('form[name="forgotPassword"]').find('button[type="submit"]').get(0);
	loading(btn);
	var arr = $('form[name="forgotPassword"]').serializeArray();
	var errLabel = $('form[name="forgotPassword"]').closest('.login-credentials').find('.errMsg').get(0);
	$('.login-credentials form .form-group').removeClass('inValid');
	$(errLabel).removeClass('inValid');
	$(errLabel).removeClass('success');
	$(errLabel).text('');
	var data = {};
	for (var i = 0; i < arr.length; i++) {
		var obj = arr[i];
		data[obj.name] = obj.value;
	}
	var url = $('form[name="forgotPassword"]').attr('action');
	var successMsg = "A link has been sent to your registered email to change password";
	callAjax(url, data, errLabel, btn, null, successMsg);
}

function updateProfile(){
	var btn = $('form[name="updateProfile"]').find('button[type="submit"]').get(0);
	loading(btn);
	var arr = $('form[name="updateProfile"]').serializeArray();
	var errLabel = $('form[name="updateProfile"]').closest('.login-credentials').find('.errMsg').get(0);
	$('.login-credentials form .form-group').removeClass('inValid');
	$(errLabel).removeClass('inValid');
	$(errLabel).removeClass('success');
	$(errLabel).text('');
	var data = {};
	for (var i = 0; i < arr.length; i++) {
		var obj = arr[i];
		data[obj.name] = obj.value;
	}
	var number = $('form[name="updateProfile"]').find('.selected-dial-code').text();
	if(number.charAt(0) == '+'){
		number = number.substr(1);
	}
	data['country_code'] = number;
	var url = $('form[name="updateProfile"]').attr('action');
	var successMsg = "Your profile has been updated successfully!";
	callAjax(url, data, errLabel, btn, null, successMsg);
}

function loading(btn){
	$(btn).find('.glyphicon-refresh-animate').addClass('loading');
	$(btn).find('i').text('Loading');
	$(btn).addClass('inValid');
}

function stopLoading(btn){
	var text = $(btn).find('i').data('text');
	$(btn).find('.glyphicon-refresh-animate').removeClass('loading');
	$(btn).find('i').text(text);
	$(btn).removeClass('inValid');	
}

function callAjax(url, data, errLabel, btn, href, successMsg, action) {
	var formValid = validateFormData(data, errLabel);
	// console.log(url, data);
	// console.log(formValid);
	if(formValid){
		$.ajax({
			url: url,
			method: "POST",
			dataType: "JSON",
			contentType: "application/json",
			data: JSON.stringify(data),
			success: function(response){
				// console.log(response);
				if(action) {
        			ga('send', 'event', action, 'Submit');
				}

				if(action == 'SIGNUP') {
					 location.reload(true);
				}

				if(href){
					window.location.href = href;
				}else if(successMsg){
					displaySuccessMsg(errLabel, successMsg);
				}
				stopLoading(btn);
			},
			error: function(err){
				console.log(err);
				if(err.status === 401){
					var responseText = JSON.parse(err.responseText);
					var msg = responseText.message;
				}else if(err.status === 500){
					var msg = 'Something went wrong. Please try again';
				}else if(err.status == 422){
					var msg = '';
					var userId = '';
					var responseText = JSON.parse(err.responseText);
					if(typeof(responseText) === "object"){
						var keys = Object.keys(responseText);
						if(keys.length > 0){
							msg = responseText[keys[0]];
							if(msg == 'Account not yet validated. Please check your email.') {
								userId = responseText[keys[1]];
							}
						}
						for (var i = 0; i < keys.length; i++) {
							var key = keys[i];
							$('.login-credentials form .form-control[name="'+key+'"]').closest('.form-group').addClass('inValid');
						}
					}
				}
				if(msg == 'Account not yet validated. Please check your email.') {
					displayResendActivationButton(userId, action);
				}
				displayErr(errLabel, msg);
				stopLoading(btn);
			}
		});
	}else{
		stopLoading(btn);
	}
}

function displayResendActivationButton(Id, action) {
	if(action == 'LOGIN') {
		$('#activation_email_button_login').css('display', 'block');
		$('#activation_email_link_login').val(Id);
	} else {
		$('#activation_email_button_signup').css('display', 'block');
		$('#activation_email_link_signup').val(Id);
	}
}
function displaySuccessMsg(errLabel, msg){
	$('#activation_email_button').css('display', 'none');
	$(errLabel).removeClass('inValid').addClass('success');
	$(errLabel).text(msg);
	setTimeout(function(){
		$('.login-credentials').modal('hide');
		$(errLabel).removeClass('inValid');
		// $(errLabel).removeClass('success');
		// $(errLabel).text('');
		if(typeof (redirectLanding) !== 'undefined') {
			window.location.href = redirectLanding;
		}
		clearForms();
	}, 3000);
}

function displayErr(errLabel, msg){
	$(errLabel).text(msg);
	$(errLabel).addClass('inValid');
}

function clearForms(){
	$('.login-credentials form').each(function(i, form){
		var formName = $(form).attr('name');
		if(formName != 'updateProfile'){
			$(form).find("input, textarea").val("");
		}
	});
}

function validateFormData(data, errLabel){
	var msg = null;
	var keys = Object.keys(data);
	if($.inArray('email', keys) != -1){
		if(!(data.email && isValidEmailAddress(data.email))){
			msg = 'Enter valid Email';
			displayErr(errLabel, msg);
			return false;
		}
	}
	if($.inArray('password', keys) != -1){
		if(!(data.password && data.password.length >= 3)){
			msg = 'Enter valid password';
			displayErr(errLabel, msg);
			return false;
		}
	}
	if($.inArray('first_name', keys) != -1){
		if(!(data.first_name && isValidName(data.first_name))){
			msg = 'Enter valid First Name';
			displayErr(errLabel, msg);
			return false;
		}
	}
	// if($.inArray('last_name', keys) != -1){
	// 	if(!(data.last_name && isValidName(data.last_name))){
	// 		msg = 'Enter valid Last Name';
	// 		displayErr(errLabel, msg);
	// 		return false;
	// 	}
	// }
		// if($.inArray('phone_number', keys) != -1){
		// 	if(!(data.phone_number && isValidPhoneNumber(data.phone_number))){
		// 		msg = 'Enter valid Phone Number';
		// 		displayErr(errLabel, msg);
		// 		return false;
		// 	}
		// }
	return true;
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

function isValidName(name){
	if ( name.match(/^[A-Za-z\s]+$/) ) {
        return true;
    } else {
        return false
    }
}

function isValidPhoneNumber(number){
	if ( name.match(/^\d*$/) ) {
        return true;
    } else {
        return false
    }
}

	function fetchAndUpdateUserLocation() {
    	$.ajax({
			method: 'GET',
			url: 'https://freegeoip.net/json/?callback=',
		}).done(function(result) {
			$('.user_city').val(result.city);
			$('.user_state').val(result.region_name);
			$('.user_country').val(result.country_name);
		});
	}





