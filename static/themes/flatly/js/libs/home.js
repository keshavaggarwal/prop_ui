
$(document).ready(function(){
	$('.landing').removeClass('visibility-hidden');
	$('#news-carousel').carousel({
	    interval: 3000
	});
    animateOnLoad();
    animate()
     Carousel.init({
        target: $('.carousel')
    });

	var distance = $('.how-it-works').offset().top,
	    $window = $(window);

	distance = distance - $('.header').height();
	$window.scroll(function() {
	    if ( $window.scrollTop() >= distance ) {
	        $('.header').addClass('blue-brgd');
	    }
	    // else{
	    // 	$('.header').removeClass('blue-brgd');
	    // }
	});

	$('.people-details .images img').click(function(){
		if(!$(this).hasClass('active')){
			var data = $(this).data();
			$(this).addClass(data.keyframe);
			(function(ele){
				setTimeout(function(){
					$(ele).removeClass(data.keyframe);
				}, data.duration * 1000 );
			})($(this))

			$('.founders-details.active').fadeOut();
			$('.founders-details').removeClass('active');

			$('.founders-details.'+data.name).addClass('active');
			$('.founders-details.active').fadeIn(1000);

			$('.people-details .images img').removeClass('active');
			$('.people-details .images img.'+data.name).addClass('active');

		}
	});

	if(screen.width > 768){
		isScrolling();
	}
});

function isScrolling(){
	$(window).scroll(function (){
		if(isScrolledIntoView($('.big-triangle img.tri-big'))){
			// $('.image-container .building').removeClass('hide');
			$('.image-container').addClass('animate');
		}
	})
}

function rendered( carousel ) {
        title.text( carousel.nearestItem().element.alt )

        // Fade in based on proximity of the item
        var c = Math.cos((carousel.floatIndex() % 1) * 2 * Math.PI)
        title.css('opacity', 0.5 + (0.5 * c))
      }
function animateOnLoad(){
	$('.number-animate-onload').each(function(i, ele){
		applyAnimate(ele);
	});
}

function animate(){
	// var percent_number_step = $.animateNumber.numberStepFactories.append('');
	$('.number-animate').each(function(i, ele){
		(function(ele){
			var done = true;
			$(window).scroll(function () {
				if(isScrolledIntoView($(ele)) && done){
					applyAnimate(ele)
					done = false;
				}
			})
		})(ele)
	});
}

function applyAnimate(ele){
	var number = $(ele).data();
	$(ele).animateNumber({
	    number: number.number,
	    easing: 'easeInQuad',
	},1000);
}

function isScrolledIntoView(elem){
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom));
}