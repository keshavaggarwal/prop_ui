var app = new Vue({
	el: '#app',
	components: {
		'my-component' : {
			 template: '#template',
			 data: function() {
				return {
					list: [],
					category : 'commercial',
					isDebt : 0,
					isBid : 0,
					city: 'all',
					sortBy: 'created_at',
					date: ''
				};
			},
			watch: {
			    list:function(){
			   		setTimeout(function () {
			   		$('.card-slider').slider({
						formatter: function(value) {
							return value + '%';
						}
					});
					$('[data-toggle="popover"]').popover({
						container: 'body'
					});
				}, 300);
			    }
			},
			methods: {
				isFeature: function(file) {
					return file.pivot.zone.indexOf('features_') >= 0;
				},
				findFeatureKey: function(file, property) {
					var key = file.pivot.zone.replace("features_", "");
					if (typeof property['features'][key-1] === 'undefined') {
						return;
					}
					return property['features'][key-1]['title'] 
				},
				getPropertyUrl: function(property) {
					if(property.status == 'coming_soon') {
						return "javascript:void(0)";
					}
					var category = property.category;
					var id = property.id;
					var url = '';
					if(category == 'residential') {
						url = 'properties/residentialproperties/'+id+'/view';
						return url;
					}
					url = 'properties/commercialproperties/'+id+'/view';
					return url;
				},
				dateDiff: function(date) {
					    t1= this.date;
					    t2= date; //date2

					    var one_day=1000*60*60*24; 

					    var x=t1.split("-");     
					    var y=t2.split("-");

					    var date1=new Date(x[0],(x[1]-1),x[2]);
					    var date2=new Date(y[0],(y[1]-1),y[2]);

					    var month1=x[1]-1;
					    var month2=y[1]-1;

					    _Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day));
					    return _Diff;
				},
				getFundedPercentage: function(item) {
					if(item.type == 'debt') {
						return  ((Number(item.funded * 100) / Number(item.debt.loan_amount)).toFixed(1)*100)/100;
					}

                                        var financialModelInvestment = 0;
                                        $.each(item.model.financial_model,function(key,value){
                                            financialModelInvestment += parseInt(value.investment);
                                        });

					return  ((Number(item.funded * 100) / Number(financialModelInvestment)).toFixed(1)*100)/100;
				},
				convertToIndianSystem: function(x){
					if(x){
				    	x=x.toString();
				        var afterPoint = '';
				        if(x.indexOf('.') > 0)
				           afterPoint = x.substring(x.indexOf('.'),x.length);
				        x = Math.floor(x);
				        x=x.toString();
				        var lastThree = x.substring(x.length-3);
				        var otherNumbers = x.substring(0,x.length-3);
				        if(otherNumbers != '')
				            lastThree = ',' + lastThree;
				        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
				        return res;

					}else
						return 0;
				},
				getBidProperties: function() {
					this.isBid = 1;
					this.makeNewList();
				},
				getNewProperties: function() {
					this.isBid = 0;
					this.makeNewList()
				},
				getDebtProperties: function() {
					$('#selected_city').val('all').trigger('change.select2');
					$('#sortBy').val('created_at').trigger('change.select2');
					$('#new_properties').addClass('active');
					$('#bid_properties').removeClass('active');
					$('#shadow').css('left', '0%');
					this.isDebt = 1;
					this.isBid = 0;
					this.makeNewList();
				},
				updateCategory: function(category) {
					$('#selected_city').val('all').trigger('change.select2');
					$('#sortBy').val('created_at').trigger('change.select2');
					$('#new_properties').addClass('active');
					$('#bid_properties').removeClass('active');
					$('#shadow').css('left', '0%');
					this.category = category;
					this.isDebt = 0;
					this.isBid = 0;
					this.makeNewList();
				},
				getCapitalType: function(type) {
					if(type == 'debt') {
						return 'DEBT';
					}
					return 'EQUITY';
				},
				getImageUrlByType: function(type) {
					if(type == 'debt') {
						return "assets/media/debt.png";
					}
					return "assets/media/equity.png";
				},
				makeNewList: function() {
					this.list = [];
					data = {};
					$('#loading_text').html('Loading..');
					$('#loading_gif').css('display', 'block');
					data['city'] = $('#selected_city').val();
					data['sortBy'] = $('#sortBy').val();
					data['isDebt'] = this.isDebt;
					data['isBid'] = this.isBid;
					data['category'] = this.category;
					var vm = this;
					$.ajax({
						url: '/api/properties/property-list-query',
						method: 'GET',
						data: data,
						// dataType: "JSON",
						// contentType: "application/json",
						success: function(response){
							vm.list = JSON.parse(response);
							if(vm.list.length == 0) {
								$('#selected_city').val('all').trigger('change.select2');
								$('#sortBy').val('created_at').trigger('change.select2');
								$('#new_properties').addClass('active');
								$('#bid_properties').removeClass('active');
								$('.shadow').css('left', '0%');
								$('#no_results_found').css('display','block');
								setTimeout(function () {
									$('#no_results_found').css('display','none');
								}, 5000);
								vm.isBid = 0;
								vm.makeNewList();
							}
						},
						error: function(err){
							console.log(err)
						}
					})
				},
				getListingIconsNData: function (property) {
					var model_slider_details = property.slider.model_slider_details;
					var model_slider_values = property.slider.model_slider_values;
					var expectedObj = model_slider_values[model_slider_details['expected']];
					var obj ={};
					if(property.type == "equity"){
						obj[1] = {};
						obj[1]['name'] = 'RENTAL YIELD';
						obj[1]['img_name'] = 'rental-yield-black';
						obj[1]['value'] = (parseFloat(property.rental.yield).toFixed(2)*100)/100 + '%';
						obj[1]['queryText'] = 'Rent / Property Value';
						obj[1]['queryPos'] = 'top';

						obj[2] = {};
						obj[2]['name'] = 'RETURN TARGET';
						obj[2]['img_name'] = 'return-target-black';
						obj[2]['value'] = expectedObj.irr;

						obj[3] = {};

						obj[3]['name'] = 'MIN INVESTMENT';
						obj[3]['img_name'] = 'minimum-investment-black';
						obj[3]['value'] = "&#x20B9;" + this.convertToIndianSystem(property.minimum_investment);
					}
					else if(property.type == "debt"){
						obj[1] = {};
						obj[1]['name'] = 'INTEREST';
						obj[1]['img_name'] = 'interest-rate-black';
						obj[1]['value'] = Math.round(property.debt.interest_rate, 1) + '%';

						obj[2] = {};
						obj[2]['name'] = 'TENURE';
						obj[2]['img_name'] = 'tenure-black';
						var yr = (property.debt.tenture > 1) ? ' YEARS' : ' YEAR';
						obj[2]['value'] = Math.round(property.debt.tenture, 1) + yr;

						obj[3] = {};
						obj[3]['name'] = 'LTV';
						obj[3]['img_name'] = 'ltv-black';
						obj[3]['value'] = Math.round(( property.debt.loan_amount / property.pricing.purchase_price) * 100, 1) + '%';
						obj[3]['queryText'] = 'Loan to Value (Loan / Property Value)';
						obj[3]['queryPos'] = 'left';
					}
					return obj;
				},
			},
			created: function() {
					var vm 		= this;
					var now 	= new Date();
					var day 	= ("0" + now.getDate()).slice(-2);
					var month 	= ("0" + (now.getMonth() + 1)).slice(-2);
					
					var today 	= now.getFullYear() + "-" + (month) + "-" + (day);
					this.date 	= today;
					let search	= window.location.search;
					let url 	= '';

					if(search.indexOf('invest') >= 0 || search.indexOf('borrow') >= 0)
						url	+= '/api/properties/property-list?option=invest';
					else if(search.indexOf('lend') >= 0)
						url += '/api/properties/property-list?option=lend';
					else
						url += '/api/properties/property-list?option=invest';

					$.ajax({
						url: url,
						method: 'GET',
						// dataType: "JSON",
						// contentType: "application/json",
						success: function(response){
							vm.list = JSON.parse(response);
						},
						error: function(err){
							console.log(err)
						}
					});
				}
			}
		}	
})


$('.select').on("change", function (e) { app.$refs.main.makeNewList(); });


